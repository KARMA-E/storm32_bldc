/*
 * cos.c
 *
 *  Created on: Jan 25, 2022
 *      Author: KARMA
 */
#include "stm32f1xx_hal.h"
#include "tim.h"

#include "cos_table.h"
#include "motor.h"

#define _ANGLE_PERIOD			(3600)
#define _ANGLE_RANGE_MAX		(_COS_TABLE_ANGLE_RANGE)
#define _ANGLE_PHASE_OFFSET		(1200)

#define _MOTOR_QTY				(3)
#define _PHASE_QTY				(3)

#define _TIM_CCR_MAX			(1600)


typedef struct {
	__IO uint32_t * Ccr;
	int16_t Angle;
}phase_t;

typedef struct {
	uint16_t Power;
	phase_t	Phase[_PHASE_QTY];
}motor_t;


static motor_t _motor_arr[_MOTOR_QTY];


static inline int16_t _GetCos(int16_t angle)
{
	if(angle < 0)
	{
		angle *= -1;
	}

	if(angle > _ANGLE_RANGE_MAX)
	{
		angle = (_ANGLE_RANGE_MAX * 2) - angle;
	}

	return _cos_val[angle];
}

static inline void _AllPhasesRotate(motor_t * motor, int16_t angle)
{
	for(uint8_t phaseInd = 0; phaseInd < _MOTOR_QTY; phaseInd++)
	{
		phase_t * phase = &(motor->Phase[phaseInd]);
		phase->Angle += angle;

		while(phase->Angle > _ANGLE_PERIOD)
		{
			phase->Angle -= _ANGLE_PERIOD;
		}

		while(phase->Angle < (-1 * _ANGLE_PERIOD))
		{
			phase->Angle += _ANGLE_PERIOD;
		}
	}
}

static inline void _TimersInit(void)
{
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
	HAL_TIM_Base_Start(&htim4);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

	_motor_arr[0].Phase[0].Ccr = &(TIM3->CCR2);
	_motor_arr[0].Phase[1].Ccr = &(TIM3->CCR3);
	_motor_arr[0].Phase[2].Ccr = &(TIM3->CCR4);

	_motor_arr[1].Phase[0].Ccr = &(TIM2->CCR3);
	_motor_arr[1].Phase[1].Ccr = &(TIM2->CCR4);
	_motor_arr[1].Phase[2].Ccr = &(TIM3->CCR1);

	_motor_arr[2].Phase[0].Ccr = &(TIM4->CCR3);
	_motor_arr[2].Phase[1].Ccr = &(TIM2->CCR2);
	_motor_arr[2].Phase[2].Ccr = &(TIM4->CCR4);
}


void MOTOR_Init(uint16_t startPower)
{
	_TimersInit();

	for(uint8_t motorInd = 0; motorInd < _MOTOR_QTY; motorInd++)
	{
		motor_t * motor = &_motor_arr[motorInd];
		motor->Power = startPower;

		for(uint8_t phaseInd = 0; phaseInd < _MOTOR_QTY; phaseInd++)
		{
			motor->Phase[phaseInd].Angle = phaseInd * _ANGLE_PHASE_OFFSET;
		}

		MOTOR_Rotate(motorInd, 0);
	}
}

void MOTOR_Rotate(uint8_t motorInd, int16_t angle)
{
	motor_t * motor = &_motor_arr[motorInd];

	_AllPhasesRotate(motor, angle);

	for(uint8_t phaseInd = 0; phaseInd < _MOTOR_QTY; phaseInd++)
	{
		__IO uint32_t * ccr = motor->Phase[phaseInd].Ccr;
		int16_t phaseAngle  = motor->Phase[phaseInd].Angle;

		*ccr = ((motor->Power * (int32_t)_GetCos(phaseAngle)) / 100) + (_TIM_CCR_MAX / 2);
	}
}
