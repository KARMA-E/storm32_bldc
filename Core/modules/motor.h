/*
 * cos.h
 *
 *  Created on: Jan 25, 2022
 *      Author: KARMA
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include <stdint.h>

void MOTOR_Init(uint16_t startPower);
void MOTOR_Rotate(uint8_t motorInd, int16_t angle);

#endif
