/*
 * mpu6050.c
 *
 *  Created on: Jan 26, 2022
 *      Author: KARMA
 */

#include "stm32f1xx_hal.h"
#include "mpu6050.h"
#include "i2c.h"


#define _I2C_WRITE_BIT		(0x0)
#define _I2C_READ_BIT		(0x1)
#define _AXIS_QTY			(3)


static inline bool _StartSeq(I2C_TypeDef * I2C, uint8_t devAddr, uint8_t rwBit)
{
	I2C->CR1 &= ~I2C_CR1_POS;
	I2C->CR1 |= I2C_CR1_START;
	while((I2C->SR1 & I2C_SR1_SB) != I2C_SR1_SB){}

	(void) I2C->SR1;

	I2C->DR = (devAddr << 1) | rwBit;

	while((I2C->SR1 & I2C_SR1_ADDR) != I2C_SR1_ADDR)
	{
		if((I2C->SR1 & I2C_SR1_AF) == I2C_SR1_AF)
		{
			I2C->SR1 &= ~I2C_SR1_AF;
			I2C->CR1 |= I2C_CR1_STOP;
			return false;
		}
	}

	(void) I2C->SR1;
	(void) I2C->SR2;

	return true;
}

static bool _WriteBuf(I2C_TypeDef * I2C, uint8_t devAddr, uint8_t regAddr, uint8_t * buf, uint16_t size)
{
	if(!_StartSeq(I2C, devAddr, _I2C_WRITE_BIT))
	{
		return false;
	}

	I2C->DR = regAddr;
	while((I2C->SR1 & I2C_SR1_TXE) != I2C_SR1_TXE){}

	for(uint16_t ind = 0; ind < size; ind++)
	{
		I2C->DR = *buf++;
		while((I2C->SR1 & I2C_SR1_TXE) != I2C_SR1_TXE){}
	}

	I2C->CR1 |= I2C_CR1_STOP;

	return true;
}

static bool _ReadBuf(I2C_TypeDef * I2C, uint8_t devAddr, uint8_t regAddr, uint8_t * buf, uint16_t size)
{
	I2C->CR1 |= I2C_CR1_ACK;

	if(!_StartSeq(I2C, devAddr, _I2C_WRITE_BIT))
	{
		return false;
	}

	I2C->DR = regAddr;
	while((I2C->SR1 & I2C_SR1_TXE) != I2C_SR1_TXE){}

	if(!_StartSeq(I2C, devAddr, _I2C_READ_BIT))
	{
		return false;
	}

	for(uint16_t ind = 0; ind < (size - 1); ind++)
	{
		while((I2C->SR1 & I2C_SR1_RXNE) != I2C_SR1_RXNE){}
		*buf++ = I2C->DR;
	}

	I2C->CR1 &= ~I2C_CR1_ACK;
	I2C->CR1 |= I2C_CR1_STOP;

	while((I2C->SR1 & I2C_SR1_RXNE) != I2C_SR1_RXNE){}
	*buf = I2C->DR;

	return true;
}


uint8_t MPU6050_GetId(uint8_t devAddr)
{
	uint8_t data = 0;
	_ReadBuf(I2C2, devAddr, 0x75, &data, sizeof(data));
	return data;
}

void MPU6050_WriteReg(uint8_t devAddr, uint8_t regAddr, uint8_t data)
{
	_WriteBuf(I2C2, devAddr, regAddr, &data, 1);
}

void MPU6050_GetAcc(uint8_t devAddr, int16_t * accVal)
{
	uint8_t buf[_AXIS_QTY * 2];

	_ReadBuf(I2C2, devAddr, 0x3B, buf, _AXIS_QTY * 2);

	for(uint8_t axis = 0; axis < _AXIS_QTY; axis++)
	{
		accVal[axis] = (int16_t)((uint16_t)buf[axis * 2] << 8) + buf[(axis * 2) + 1];
	}
}


