/*
 * mpu6050.h
 *
 *  Created on: Jan 26, 2022
 *      Author: KARMA
 */

#ifndef MPU6050_H_
#define MPU6050_H_

#include <stdbool.h>

#define MPU6050_DEV1_ADDR	(0x68)

uint8_t MPU6050_GetId(uint8_t devAddr);
void	MPU6050_WriteReg(uint8_t devAddr, uint8_t regAddr, uint8_t data);
void 	MPU6050_GetAcc(uint8_t devAddr, int16_t * accVal);

#endif
